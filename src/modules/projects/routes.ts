import ProjectIndex from "@/modules/projects/views/Index.vue";
import ProjectHome from "@/modules/projects/views/Home.vue";
import ProjectView from "@/modules/projects/views/View.vue";
import ProjectViewIndex from "@/modules/projects/views/ViewIndex.vue";
import ProjectNotes from "@/modules/projects/views/notes/Index.vue";

const routes: Array<any> = [
  {
    path: "/projects",
    component: ProjectIndex,
    children: [
      {
        path: "",
        name: "projects-home",
        component: ProjectHome
      },
      {
        path: ":uuid",
        component: ProjectViewIndex,
        children: [
          {
            path: "",
            name: "project-view",
            component: ProjectView
          },
          {
            path: "notes",
            name: "project-notes",
            component: ProjectNotes
          }
        ]
      }
    ]
  }
];

export default routes;
