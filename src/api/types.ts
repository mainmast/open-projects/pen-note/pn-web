export interface Request {
  endpoint: string;
  params?: object;
  data?: object;
  headers?: object;
  withCredentials?: boolean;
}

export interface Config {
  params?: object;
  headers?: object;
  withCredentials?: boolean;
}
