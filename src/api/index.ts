import axios, { AxiosInstance } from "axios";
import { Request, Config } from "@/api/types";

export const BaseURL = "http://localhost:8000/";

const setAuth = (http: AxiosInstance, context: any) => {
    const token = context.getters.getAccessToken;
    http.defaults.headers.common["Authorization"] = "Bearer " + token;
    http.defaults.withCredentials = true;

    return http;
};

export const $get = async (context: any, request: Request) => {
    const config: Config = {
        params: request.params,
        headers: request.headers,
        withCredentials: request.withCredentials || false
    };
    const http = setAuth(axios, context);

    return http
        .get(request.endpoint, config)
        .then(response => [null, response])
        .catch(error => [error]);
};

export const $post = async (context: any, request: Request) => {
    const config: Config = {
        params: request.params,
        headers: request.headers,
        withCredentials: request.withCredentials || false
    };
    const http = setAuth(axios, context);

    return http
        .post(request.endpoint, request.data, config)
        .then(response => [null, response])
        .catch(error => [error]);
};

export const $patch = async (request: Request) => {
    const config: Config = {
        params: request.params,
        headers: request.headers,
        withCredentials: request.withCredentials || false
    };

    return axios
        .post(request.endpoint, request.data, config)
        .then(response => [null, response])
        .catch(error => [error]);
};

export const $put = async (request: Request) => {
    const config: Config = {
        params: request.params,
        headers: request.headers,
        withCredentials: request.withCredentials || false
    };

    return axios
        .post(request.endpoint, request.data, config)
        .then(response => [null, response])
        .catch(error => [error]);
};

export const $delete = async (request: Request) => {
    const config: Config = {
        params: request.params,
        headers: request.headers,
        withCredentials: request.withCredentials || false
    };

    return axios
        .get(request.endpoint, config)
        .then(response => [null, response])
        .catch(error => [error]);
};
