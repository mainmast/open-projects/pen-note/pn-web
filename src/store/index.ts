import Vue from "vue";
import Vuex from "vuex";

import { BaseURL, $post } from "../api/index";
import { Request } from "../api/types";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    loggedIn: false,
    token: {
      access: "" as string,
      refresh: "" as string
    }
  },
  getters: {
    getAccessToken: state => {
      if (state.loggedIn) {
        return state.token.access;
      }

      return sessionStorage.getItem("jwtSessionId");
    },
    isLoggedIn: state => {
      return state.token.access !== null || sessionStorage.getItem("jwtSessionId") !== null;
    }
  },
  mutations: {
    setTokens(state, data) {
      state.token.access = data.access;
      state.token.refresh = data.refreshToken;
      state.loggedIn = true;
    }
  },
  actions: {
    async loginUser(context, data) {
      const request: Request = {
        endpoint: BaseURL + "api/token/",
        data: data
      };

      const [err, result] = await $post(context, request);

      if (err) {
        return [err, null];
      }

      context.commit("setTokens", result.data);

      // TODO:!!!! REMOVE THIS IN PLACE OF SECURE COOKIE
      sessionStorage.setItem("jwtSessionId", result.data.access);
      sessionStorage.setItem("jwtRefreshId", result.data.refreshToken);

      return [err, result];
    }
  }
});
