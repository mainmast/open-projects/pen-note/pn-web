import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";

// import plugin
import { TiptapVuetifyPlugin } from "tiptap-vuetify";

// don't forget to import CSS styles
import "tiptap-vuetify/dist/main.css";

// use this package's plugin
Vue.use(TiptapVuetifyPlugin, {
  vuetify,
  iconsGroup: "mdi"
});

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount("#app");
