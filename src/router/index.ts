import Vue from "vue";
import VueRouter from "vue-router";
import Login from "@/views/Login.vue";

import ProjectRoutes from "@/modules/projects/routes";

Vue.use(VueRouter);

const baseRoutes = [
  {
    path: "/",
    redirect: {
      name: "login"
    }
  },
  {
    path: "/login",
    name: "login",
    component: Login,
    meta: {
      loggedIn: false as boolean
    }
  }
];

const routes = baseRoutes.concat(ProjectRoutes);

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
